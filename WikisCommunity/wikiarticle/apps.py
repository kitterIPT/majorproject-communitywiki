from django.apps import AppConfig


class WikiarticleConfig(AppConfig):
    name = 'wikiarticle'
