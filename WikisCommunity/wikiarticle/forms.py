from django.forms import ModelForm
from .models import Article, Comment,ArticleProperty
from django import forms


class ArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'header_image', 'content_article', 'page_category' ]


class ArticlePropertyForm(ModelForm):
    class Meta:
        model = ArticleProperty
        fields = ['visibility', 'status']


class EditArticleForm(ModelForm):
    class Meta:
        model = Article
        fields = ['content_article']


class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['content', 'anonymous']


class SearchArticleForm(forms.Form):
        query = forms.CharField(label='')


