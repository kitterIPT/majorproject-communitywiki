from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from .forms import ArticleForm, CommentForm, EditArticleForm, SearchArticleForm, ArticlePropertyForm
from .models import Article, ArticleProperty, ArticleStats
from django.views.generic import DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import TrigramSimilarity
from wikipage.models import WikiCategory, WikiPage, PageCategory
from django import template




@login_required
def create_article(request, id, page_slug):
    wiki_page = get_object_or_404(WikiPage, slug=page_slug)
    page_categories = PageCategory.objects.filter(page=wiki_page)
    if request.method == 'POST':
        form = ArticleForm(request.POST,request.FILES)
        if form.is_valid():
            new_article = form.save(commit=False)
            new_article.is_author = request.user
            new_article.wiki_page = wiki_page
            new_article.save()
            article_property = ArticleProperty.objects.create(article=new_article)
            article_stats = ArticleStats.objects.create(article=new_article)
            article_stats.save()
            article_property.save()

            return redirect('wikiarticle:detail_article', page_slug=new_article.wiki_page.slug,
                            category_slug=new_article.page_category.slug, article_slug=new_article.slug)
    else:
        form = ArticleForm(request.FILES)
    return render(request, 'wikiarticle/create.html',
                  {'form': form, 'wiki_page': wiki_page,
                   'page_categories': page_categories, })


def detail_article(request, page_slug, category_slug, article_slug):
    article = get_object_or_404(Article, slug=article_slug)
    status_article = int(article.articleproperty.status)
    # filter comments in article
    comments = article.comments.filter(active=True)
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.article = article
            if request.user.is_authenticated:
                new_comment.is_user = request.user
            article.articlestats.comment_number += 1
            new_comment.save()
    else:
        comment_form = CommentForm()
        article.articlestats.view += 1

    return render(request, 'wikiarticle/detail_article.html', {'article': article,
                                                                'comments': comments,
                                                                'comment_form': comment_form,
                                                               'status_article': status_article,})


def edit_article(request, page_slug, category_slug, article_slug):
    article = get_object_or_404(Article, slug=article_slug)
    status_article = int(article.articleproperty.status)
    property_form = None
    if request.user.is_superuser:
        property_article = get_object_or_404(ArticleProperty, article=article)
        if request.method == 'POST':
            form = EditArticleForm(request.POST, request.FILES, instance=article)
            property_form = ArticlePropertyForm(request.POST, instance=property_article)
            if form.is_valid() and property_form.is_valid():
                form.save()
                property_form.save()
                return redirect('wikiarticle:detail_article',
                                page_slug=page_slug,
                                category_slug=category_slug, article_slug=article_slug)
        else:
            property_form = ArticlePropertyForm(instance=property_article)
    else:
        if status_article == 1 or (status_article == 2 and request.user.is_authenticated):
            if request.method == 'POST':
                form = EditArticleForm(request.POST, request.FILES, instance=article)
                if form.is_valid():
                    form.save()
                    return redirect('wikiarticle:detail_article',
                                    page_slug=page_slug,
                                    category_slug=category_slug, article_slug=article_slug)
        else:
            return redirect('wikiarticle:detail_article',
                            page_slug=page_slug,
                            category_slug=category_slug, article_slug=article_slug)
    form = EditArticleForm(instance=article)
    return render(request, 'wikiarticle/edit.html', {'form': form, 'property_form': property_form, 'article': article })


def search_article(request):
    search_form = SearchArticleForm()
    query = None
    results = []
    if 'query' in request.GET:
        search_form = SearchArticleForm(request.GET)
        if search_form.is_valid():
            query = search_form.cleaned_data['query']
            results = Article.objects.annotate(
                similarity=TrigramSimilarity('title', query),).filter(similarity__gt=0.1).order_by('-similarity')
    return render(request, 'wikiarticle/search_article.html',
                  {'search_form': search_form, 'query': query, 'results': results})
