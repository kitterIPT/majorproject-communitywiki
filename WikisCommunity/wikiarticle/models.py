from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField
from wikipage.models import WikiPage
from django.contrib.auth.models import User
from  django.urls import reverse
from django.template.defaultfilters import slugify
from wikipage.models import PageCategory, WikiPage



STATUS_CHOICES = (
    ('1','NORMAL'),
    ('2', 'RESTRICT'),
    ('3','LOCK'),
)


class Article(models.Model):
	title = models.CharField(max_length=100)
	header_image = models.ImageField(blank=True, null=True, upload_to='images/%Y/%m/%d/')
	content_article = RichTextUploadingField(blank=True, null=True)
	time_stamp = models.DateTimeField(default=timezone.now)
	slug = models.SlugField(null=True, unique=True)
	wiki_page = models.ForeignKey(WikiPage, related_name='Article', on_delete=models.CASCADE)
	is_author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	page_category = models.ForeignKey(PageCategory, on_delete=models.CASCADE)

	class Meta:
		verbose_name = 'Articles'
		verbose_name_plural = 'Articles'
		ordering = ['-time_stamp']

	def __str__(self):
		return 'Article: {}'.format(self.title)

	def get_absolute_url(self):
		return reverse('wikiarticle:detail_article', args=[self.wiki_page.slug, self.page_category.slug, self.slug])

	def get_edit_absolute_url(self):
		return reverse('wikiarticle:edit_article', args=[self.wiki_page.slug, self.page_category.slug, self.slug])

	def save(self, *args, **kwargs):
		if not self.slug:
			super().save(*args, **kwargs)
			self.slug = '{}-{}'.format(slugify(self.title), self.id)
		return super().save(*args,**kwargs)


# Model show the attribute Article
class ArticleProperty(models.Model):
	article = models.OneToOneField(Article, on_delete=models.CASCADE, primary_key=True)
	visibility = models.BooleanField(default=True)
	status = models.CharField(max_length=8, choices=STATUS_CHOICES, default='1')

	class Meta:
		verbose_name = 'ArticleProperty'
		verbose_name_plural = 'ArticleProperties'

	def __str__(self):
		return 'ArticleProperty: {}'.format(self.article)


class ArticleStats(models.Model):
	article = models.OneToOneField(Article, on_delete=models.CASCADE)
	view = models.PositiveIntegerField(default=0)
	comment_number = models.PositiveIntegerField(default=0)

	class Meta:
		verbose_name = 'ArticleStats'
		verbose_name_plural = 'ArticleStats'

	def __str__(self):
		return 'ArticleStats: {}'.format(self.article)

class Comment(models.Model):
	article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comments')
	content = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
	is_user = models.ForeignKey(User, related_name='user_comment',on_delete=models.CASCADE, null=True)
	active = models.BooleanField(default=True)
	anonymous = models.CharField(blank=True, null=True, max_length=100)

	class Meta:
		ordering = ['created']

	def __str__(self):
		return 'ArticleComment: {}'.format(self.article)

