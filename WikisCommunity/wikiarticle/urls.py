from django.urls import path
from . import views

app_name = 'wikiarticle'

urlpatterns = [
    path('create/<int:id>&<slug:page_slug>', views.create_article, name='create_article'),
    path('<slug:page_slug>/<slug:category_slug>/<slug:article_slug>', views.detail_article, name='detail_article'),
    path('edit/<slug:page_slug>/<slug:category_slug>/<slug:article_slug>', views.edit_article, name='edit_article'),
    path('search/', views.search_article, name='search_article'),
]