from django.contrib import admin
from .models import *


class ArticlePropertyAdmin(admin.StackedInline):
    model = ArticleProperty
    can_delete = False
    list_display = ['visibility', 'status']
    list_filter = ['visibility', 'status'] #It's not work. RIP


class ArticleStatsAdmin(admin.StackedInline):
    model = ArticleStats
    can_delete = False
    list_display = [ 'view', 'comment_number']


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    inlines = [ArticlePropertyAdmin, ArticleStatsAdmin]
    fieldsets = [
        (None, {'fields': ['wiki_page','page_category','is_author']}),
        ('Create article', {'fields': ['title', 'slug', 'header_image', 'content_article']})
    ]
    list_display = ['title', 'slug', 'is_author', 'wiki_page', 'page_category', 'time_stamp']
    search_field = ['title', 'ArticleProperty_set__visibility', 'ArticleProperty_set__status']
    list_filter = ['wiki_page', 'page_category']
    prepopulated_fields = {'slug': ('title',)}

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['article','is_user','content', 'anonymous']
    search_fields = ['article']
    list_filter =  ['article']


