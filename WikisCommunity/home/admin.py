from django.contrib import admin
#from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
#from django.contrib.auth.models import User
from .models import *


# Define an inline admin descriptor for Anony model
# which acts a bit like a singleton


"""class AnonyUserInline(admin.StackedInline):
    model = AnonyUser
    can_delete = False
    verbose_name_plural = 'Anonymous User'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (AnonyUserInline,)
# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)"""


# Register Admin
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
	list_display = ['user', 'date_of_birth', 'photo']


@admin.register(PageRequest)
class PageRequestAdmin(admin.ModelAdmin):
	list_display = ('user', 'title', 'created', 'active')
	list_filter = ('active', 'created')
	search_fields = ('title', 'content')

