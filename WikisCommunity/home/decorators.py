from django.contrib.auth.decorators import login_required, user_passes_test

# Active user =========================================================================================
user_login_required = user_passes_test(lambda user: user.is_active, login_url='login') # Can I remove the login_url?

def active_user_required(view_func):
	decorated_view_func = login_required(user_login_required(view_func))
	return decorated_view_func


# Wiki - Role ===========================================================================================
def checkgroup(groupname):
	if groupname == 'admin':
		def _checkgroup(user):
			return user.is_superuser
	else:
		def _checkgroup(user):
			return user.groups.filter(name=groupname).exists()

	return user_passes_test(_checkgroup)