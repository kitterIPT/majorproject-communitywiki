from django.shortcuts import render
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .models import Profile, PageRequest
from .forms import LoginForm, RegisterForm, UserEditForm, ProfileEditForm, PageRequestForm


def index(request, category_slug=None): 
	return render(request, 'pages/home.html')


def policy(request):
	return render(request, 'pages/pagepolicy.html')


def login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			user = authenticate(request, username=cd['username'],
										password=cd['password'])
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponse('Login success.')
				else:
					return HttpResponse('Account is banned.')
			else:
				return HttpResponse('Invalid input.')
	else:
		form = LoginForm()
	return render(request, 'pages/login.html', {'form': form})


def register(request):
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid():
			new_user = form.save()
			Profile.objects.create(user=new_user)
			return render(request, 'account/register_done.html', 
										{'new_user': new_user})
	else:
		form = RegisterForm()
	return render(request, 'pages/register.html', {'form': form})


@login_required
def account(request):
	return render(request, 'pages/account.html', {'section': 'account'})


@login_required
def edit(request):
	if request.method == 'POST':
		user_form = UserEditForm(instance=request.user, data=request.POST)
		profile_form = ProfileEditForm(instance=request.user.profile, 
								data=request.POST, files=request.FILES)

		if user_form.is_valid() and profile_form.is_valid():
			user_form.save()
			profile_form.save()
			messages.success(request, 'Update success.')
		else:
			messages.error(request, 'Invalid input.')
	else:
		user_form = UserEditForm(instance=request.user)
		profile_form = ProfileEditForm(instance=request.user.profile)

	return render(request, 'pages/account.html', {'user_form': user_form, 
											'profile_form': profile_form})


@login_required
def page_request(request):
	requests = PageRequest.objects.filter(active=True)
	if request.method == 'POST':
		request_form = PageRequestForm(data=request.POST)
		if request_form.is_valid():
			new_request = request_form.save(commit=False)
			new_request.user = request.user
			new_request.save()
	else:
		request_form = PageRequestForm()
	return render(request, 'pages/page_request.html', {'requests': requests,  
												'request_form': request_form})


"""def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip"""