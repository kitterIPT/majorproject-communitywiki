from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile, PageRequest
import re #regular expression


class LoginForm(forms.Form):
    username = forms.CharField(label='Username or Email', max_length=30)
    password = forms.CharField(label='Password', widget=forms.PasswordInput())
    

class RegisterForm(UserCreationForm):
    def clean_email(self):
            email = self.cleaned_data['email']
            if User.objects.filter(email=email).exists():
                raise forms.ValidationError('Email already exists!')

            return email

    class Meta:
        model = User
        fields = ['username', "email", "password1", "password2"]


class UserEditForm(forms.ModelForm):
    first_name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileEditForm(forms.ModelForm):
    date_of_birth = forms.DateTimeField(label='Date of Birth(yyyy/mm/dd)',
                            required=False, 
                            widget=forms.TextInput(
                                attrs={'placeholder': 'ex: 2012-12-22'}))

    class Meta:
        model = Profile
        fields = ('date_of_birth', 'photo')

class PageRequestForm(forms.ModelForm):
    class Meta:
        model = PageRequest
        fields = ['title', 'content']