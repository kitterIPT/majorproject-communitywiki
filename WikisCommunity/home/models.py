from django.db import models
from django.contrib.auth.models import User
from django.db.models import DateTimeField
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
"""class AnonyUser(models.Model):
	AnonyUser = models.OneToOneField(User, on_delete=models.CASCADE, blank = True, null = True)
	ip_adresss = models.GenericIPAddressField(null=True)"""


class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	date_of_birth = models.DateField(blank=True, null=True)
	#city = models.CharField(max_length=100, blank=True)
	#country = models.CharField(max_length=100, blank=True)
	photo = models.ImageField(upload_to='users/%Y/%m/%d/', blank=True)
	signup_confirmation = models.BooleanField(default=False)

	def __str__(self):
		return self.user.username

	def get_author(self):
		return self.user.username


class PageRequest(models.Model):
	user = models.ForeignKey(User, related_name='user_request',on_delete=models.CASCADE, null=True)
	title = models.CharField(max_length=64)
	content = models.TextField()
	created = models.DateTimeField(auto_now_add=True)
	active = models.BooleanField(default=True)

	class Meta:
		ordering = ['-created']

	def __str__(self):
		return '{}: {}'.format(self.user.username, self.title)


"""class IPBlock(models.Model):
	ip = models.CharField(max_length=20)
	block = models.BooleanField(default=False)
	expiration = models.DateTimeField(auto_now=True)

	class Meta:
		permissions = [
			("block_ip", "Can block and remove-block a specific adress IP"),
		]


class UploadProperty(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	#path = models.ImageField(upload_to='users/.../, blank=True')"""

