from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.template.defaultfilters import slugify


STATUS_CHOICES = (
    ('1','NORMAL'),
    ('2', 'RESTRICT'),
    ('3','LOCK'),
)


class WikiCategory(models.Model):
	title = models.CharField(max_length=200, db_index=True)
	slug = models.SlugField(max_length=200, verbose_name="URL", unique=True)
	#description = models.CharField(max_length=255)

	class Meta:
		ordering = ('title',)
		verbose_name = 'Category'
		verbose_name_plural = 'Categories'

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('wikipage:page_list_by_category', args=[self.slug])

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)
		return  super().save(*args,**kwargs)





class WikiPage(models.Model):
	wiki_category = models.ForeignKey(WikiCategory, 
						related_name='wiki_pages', on_delete=models.CASCADE)
	title = models.CharField(max_length=100, db_index=True)
	slug = models.SlugField(max_length=200, verbose_name="URL", db_index=True)
	created = models.DateTimeField(default=timezone.now)

	class Meta:
		ordering = ('title',)
		index_together = (('id', 'slug'),)

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('wikipage:page_index', args=[self.id, self.slug])

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)
		return  super().save(*args,**kwargs)
		
	def create_absolute_url(self):
		return reverse('wikiarticle:create_article', args=[self.id, self.slug])



class PageProperty(models.Model):
	page = models.OneToOneField(WikiPage, related_name='page_property', on_delete=models.CASCADE)
	logo = models.ImageField(blank=True, null=True, upload_to='pages/')
	slogan = models.CharField(max_length=100)
	about = models.TextField(blank=True, null=True)
	visibility = models.BooleanField(default=True)
	status = models.CharField(max_length=8, choices=STATUS_CHOICES, default='1')

	class Meta:
		verbose_name = 'PageProperty'
		verbose_name_plural = 'PageProperties'

	def __str__(self):
		return '{}'.format(self.page)

	@property
	def get_filter_category(self, id):
		return WikiCategory.objects.filter(id=id)


class PageStats(models.Model):
	page = models.OneToOneField(WikiPage, on_delete=models.CASCADE)
	edit = models.PositiveIntegerField(verbose_name="Edit times")
	view = models.PositiveIntegerField(verbose_name="View number")
	articles = models.PositiveIntegerField(verbose_name="Aricle number")

	class Meta:
		verbose_name = 'PageStats'
		verbose_name_plural = 'PageStats'

	def __str__(self):
		return '{}'.format(self.page)


class PageCategory(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField(max_length=200, verbose_name="URL", db_index=True)
	page = models.ForeignKey(WikiPage, 
					related_name='page_categories', on_delete=models.CASCADE)

	class Meta:
		ordering = ('title',)
		verbose_name = 'PageCategory'
		verbose_name_plural = 'PageCategories'

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('wikipage:article_list', args=[self.page.id, self.page.slug, self.slug])

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)
		return  super().save(*args,**kwargs)


"""class PageLog(models.Model):
	page = models.OneToOneField(Page, on_delete=models.CASCADE)
	time_stamp = models.DateTimeField(auto_now_add=True)
	content = models.TextField()
	editor = models.CharField(max_length=100) 
"""