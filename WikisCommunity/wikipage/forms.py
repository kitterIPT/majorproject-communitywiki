from django import forms
from .models import PageProperty, PageCategory


class PageEditForm(forms.ModelForm):
    class Meta:
        model = PageProperty
        fields = ['logo', 'slogan', 'about', 'visibility', 'status']


class AddPageCategoryForm(forms.ModelForm):
	class Meta:
		model = PageCategory
		fields = ['title']


class SearchForm(forms.Form):
    query = forms.CharField(label='')
