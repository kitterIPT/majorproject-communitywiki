from django.contrib import admin
from .models import *


# Register your models here.
@admin.register(WikiCategory)
class WikiCategoryAdmin(admin.ModelAdmin):
	list_display = ['title', 'slug']
	prepopulated_fields = {'slug': ('title',)}


class PagePropertyAdmin(admin.StackedInline):
	model = PageProperty
	can_delete = False
	list_display = ['visibility', 'status']
	list_filter = ['visibility', 'status'] # It doesn't works. RIP


class PageStatsAdmin(admin.StackedInline):
	model = PageStats
	can_delete = False
	list_display = ['edit', 'view', 'article']


@admin.register(WikiPage)
class WikiPagesAdmin(admin.ModelAdmin):
	inlines = [PagePropertyAdmin, PageStatsAdmin,]
	list_display = ['title', 'slug', 'created', 'wiki_category']
	search_field = ['title', 'PageProperty_set__visibility', 
									'PageProperty_set__status'] # Well, it's doesn't works neither.
	list_filter = ['created', 'wiki_category']
	prepopulated_fields = {'slug': ('title',)}


@admin.register(PageCategory)
class PageCategoryAdmin(admin.ModelAdmin):
	list_display = ['title', 'slug', 'page']
	prepopulated_fields = {'slug': ('title',)}