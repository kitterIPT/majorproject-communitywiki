from django.urls import path
from . import views


app_name = 'wikipage'


urlpatterns = [
    path('', views.page_list, name='page_list'),
    path('search/', views.page_search, name='page_search'),
    path('<slug:wiki_category_slug>/', views.page_list, name='page_list_by_category'),
    path('<int:id>/<slug:page_slug>/', views.page_index, name='page_index'),
    path('<int:id>/<slug:page_slug>/<slug:page_category_slug>', views.page_index, name='article_list'),

    path('<slug:page_slug>/edit/', views.page_edit, name='page_edit'),
    path('<slug:page_slug>/add_category/', views.page_add_category, name='page_add_category')
]