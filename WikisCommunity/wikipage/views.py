from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.postgres.search import TrigramSimilarity
from django.contrib import messages
#from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import WikiCategory, WikiPage, PageCategory, PageProperty
from .forms import SearchForm, PageEditForm, AddPageCategoryForm
from wikiarticle.models import Article, ArticleProperty
from home.decorators import checkgroup


def page_list(request, wiki_category_slug=None):
    wiki_category = None
    wiki_categories = WikiCategory.objects.all()
    object_list = WikiPage.objects.all() #filter(available=True)
    if wiki_category_slug:
        wiki_category = get_object_or_404(WikiCategory, slug=wiki_category_slug)
        object_list = WikiPage.objects.filter(wiki_category=wiki_category)
    paginator = Paginator(object_list, 9)
    page = request.GET.get('page')
    try:
        wiki_pages = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        wiki_pages = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        wiki_pages = paginator.page(paginator.num_pages)

    return render(request, 'wikipage/list.html', {'page': page, 
                                                'wiki_category': wiki_category, 
                                                'wiki_categories': wiki_categories, 
                                                'wiki_pages': wiki_pages})


def page_index(request, id, page_slug, page_category_slug=None):
    wiki_page = get_object_or_404(WikiPage, id=id, slug=page_slug)
    page_property = PageProperty.objects.get(page=wiki_page)
    page_category = None
    page_categories = PageCategory.objects.filter(page=wiki_page)
    object_list = Article.objects.filter(wiki_page=wiki_page, articleproperty__visibility=True)
    if page_category_slug:
        page_category = get_object_or_404(PageCategory, slug=page_category_slug)
        object_list = Article.objects.filter(page_category=page_category, articleproperty__visibility=True)

    paginator = Paginator(object_list, 3)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    except EmptyPage:
        articles = paginator.page(paginator.num_pages)

    return render(request, 'wikipage/index.html', {'page': page, 
                                                'wiki_page': wiki_page, 
                                                'page_property': page_property, 
                                                'page_category': page_category, 
                                                'page_categories': page_categories, 
                                                'articles': articles})

@checkgroup('admin' or 'manager' or 'supporter' or 'page_owner' or 'page_manager')
def page_add_category(request, page_slug):
    wiki_page = get_object_or_404(WikiPage, slug=page_slug)
    page_property = PageProperty.objects.get(page=wiki_page)
    page_categories = PageCategory.objects.filter(page=wiki_page)
    if request.method == 'POST':
            form = AddPageCategoryForm(data=request.POST)
            if form.is_valid():
                new_page_category = form.save(commit=False)
                new_page_category.page = wiki_page
                new_page_category.save()
                return redirect('wikipage:page_index', id=wiki_page.id,
                                page_slug=wiki_page.slug)
    else:
            form = AddPageCategoryForm()

    return render(request, 'wikipage/add_category.html', {'add_category_form': form, 
                                                        'page_categories' : page_categories,
                                                        'page_property': page_property,
                                                        'wiki_page': wiki_page,
                                                        'page_slug': page_slug})


@checkgroup('admin' or 'manager' or 'supporter' or 'page_owner')
def page_edit(request, page_slug):
    wiki_page = get_object_or_404(WikiPage, slug=page_slug)
    page_property = PageProperty.objects.get(page=wiki_page)
    page_categories = PageCategory.objects.filter(page=wiki_page)
    if request.method == 'POST':
        edit_form = PageEditForm(request.POST, request.FILES, instance=page_property)
        if edit_form.is_valid():
            edit_page = edit_form.save(commit=False)
            edit_page.page = wiki_page
            edit_page.save()
            messages.success(request, 'Edit page property success.')
        else:
            messages.error(request, 'Invalid input.')

    else:
        edit_form = PageEditForm(instance=page_property)

    return render(request, 'wikipage/page_edit.html', {'edit_form': edit_form, 
                                                    'page_categories': page_categories,
                                                    'page_property': page_property,
                                                    'wiki_page': wiki_page,
                                                    'page_slug': page_slug})
    


def page_search(request):
    form = SearchForm()
    query = None
    object_list = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']

            object_list = WikiPage.objects.annotate(
                similarity=TrigramSimilarity('title', query),
            ).filter(similarity__gt=0.1).order_by('-similarity')


    paginator = Paginator(object_list, 9)
    page = request.GET.get('page')
    try:
        results = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        results = paginator.page(1)
    except EmptyPage:
        # If page is out of range deliver last page of results
        results = paginator.page(paginator.num_pages)

    return render(request, 'wikipage/search.html', {'page':page, 'form': form, 
                                                'query': query, 'results': results})

"""
    query = form.cleaned_data['query']

    search_vector = SearchVector('wiki_category', 'title')
    search_query = SearchQuery(query)
    queryset = WikiPage.objects

    results = queryset.annotate(rank=SearchRank(search_vector, search_query),
                    similarity=TrigramSimilarity('wiki_category', search_query) 
                    + TrigramSimilarity('title', search_query),
        ).filter(similarity__gt=0.1).order_by('-rank')
"""
