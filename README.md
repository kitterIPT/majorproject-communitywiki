# MajorProject-CommunityWiki
# Python3(Django3)
--------------------------------------
|Windows-Commandline|
## Requirement-steps:
  *Required **Python 3.8' or later** to be able to function properly(and for best experiences).
  #### 1. Create and activate virtual environment:
  ```bash
    python -m venv venv
    venv\scripts\activate
  ```
  #### 2. Install requirements:
  ```bash
    pip install -r requirements.txt
  ```
  #### 3. Run application:
  *Change directory inside project to execute manage.py
  ```bash
    python manage.py runserver
  ```
    
## Manage database:
  *Change directory inside project to execute manage.py
  #### Create base Migrate:
  ```bash
    python manage.py migrate
  ```
  #### Make migration in each application
  ```bash
    python manage.py makemigrations [subappfolder]
  ```
  #### Apply migration again using:
  ```bash
    python manage.py migrate
  ```

## Database others matter:
  ####Database connection:
  **App using PostgreSQL.** 
  
  *You can use other database engine or other database management system but it's best to use PostgreSQL because of stability. You should make some change with the search query or remove it if you want to change DB-Engine*
  *For the easiest way, you can use built-in driver-package for SQLite. Using code below to change the DATABASE instance in **settings.py**
  ```python
    'ENGINE': 'django.db.backends.sqlite3',
    'NAME': BASE_DIR / 'db.sqlite3',
  ```
  ####Full-text search using trigram similarity:
  *You'll need to install pg_trgm extension on database, create a script and run it:
  ```javascript
    CREATE EXTENSION pg_trgm;
  ```
